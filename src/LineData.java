import java.util.ArrayList;

public class LineData {

	ArrayList<Double[]> data = new ArrayList<Double[]>();

	public void setData(double[] rgb){
		Double rgbdata[] = new Double[3];

		for(int i = 0; i < 3; i++){
			rgbdata[i] = (Double)rgb[i];
		}
		data.add(rgbdata);
	}

}
