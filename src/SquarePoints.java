import org.opencv.core.Point;

enum RGB{
	B,
	G,
	R,
}

/**
 * 範囲の左上(pUL)、右下(pDR)座標を持つ
 *
 */
public class SquarePoints {
	public Point pUL;	//point UpLeft
	public Point pDR;	//point DownRight

	public SquarePoints() {
	}

	public SquarePoints(Point pUL, Point pDR){
		set(pUL, pDR);
	}

	public void set(Point pUL, Point pDR){
		this.pUL = pUL;
		this.pDR = pDR;
	}

	public double width(){
		return pDR.x - pUL.x;
	}

	public double height(){
		return pDR.y - pUL.y;
	}
}
