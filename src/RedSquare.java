import org.opencv.core.Mat;
import org.opencv.core.Point;

public class RedSquare extends SquarePoints{
	public boolean flag;

	public RedSquare(Mat mat) {
		flag = searchRed(mat);
	}

	/**
	 * 赤い部分を探す
	 *
	 * @param mat 検索するMat形式の画像
	 * @return 検索結果が見つかったかどうか(T/F)
	 */

	private boolean searchRed(Mat mat){
		Boolean result;
		Point pUL = null;
		Point pDR = null;

		for(int i = 0; i < mat.width(); i++){
			Point pStart = null;
			Point pEnd = null;

			for(int j = 0; j < mat.height(); j++){
				double[] data = mat.get(j, i);

				//R=255, G=0, B=0
				if(checkRGB(data, 255, 0, 0)){
					//行で初めて発見
					if(pStart == null){
						pStart = new Point(i, j);
					}
					pEnd = new Point(i, j);
				}
			}
			//初めの一つ目
			if(pStart != null){
				if(pUL == null){
					pUL = pStart;
				}
				pDR = pEnd;
			}
		}

		if(pUL != null){
			set(pUL, pDR);
			result = true;
		}else{
			System.out.printf("未発見\n");
			result = false;
		}

		return result;
	}

	/**
	 * 引数の配列とRGBが一致したかどうか
	 *
	 * @param data RGBの入った配列
	 * @param R Red
	 * @param G Green
	 * @param B Brue
	 * @return 一致したかどうか(T/F)
	 */
	private static boolean checkRGB(double[] data, double R, double G, double B){
		return data[RGB.R.ordinal()] == R &&
				data[RGB.G.ordinal()] == G &&
				data[RGB.B.ordinal()] == B;
	}

}
