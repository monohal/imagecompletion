import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;

public class ImageCompletion {

	enum RGB{
		B,
		G,
		R,
	}

	public static void main(String[] args){
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat imgMat = imgLoad("seaside.png");

		ImageCompletionTest(imgMat);

		System.out.println("finish");
	}

	/**
	 * 画像をloadしてMatで返す
	 * @param filename
	 * @return
	 */
	public static Mat imgLoad(String filename){
		Mat mat =  Imgcodecs.imread(filename);
		if(mat.empty()){
			System.out.println("Can't open file. " + filename);
			System.exit(1);
		}
		System.out.printf("%s loaded!%nsize:%dx%d\n", filename, mat.width(), mat.height());
		return mat;
	}

	private static void ImageCompletionTest(Mat imgMat){

		//赤い部分の座標を取得
		RedSquare redSquare = new RedSquare(imgMat);
		if(redSquare.flag == false){
			System.out.println("Not Found Red Square");
			System.exit(0);
		}

		SquareOutSideData redOutSideData = new SquareOutSideData(imgMat, redSquare);
		//redOutSideData.printData();

		long start = System.currentTimeMillis();
		searchSimilarData(imgMat, redOutSideData);
		long end = System.currentTimeMillis();
		System.out.println("searchSimilarData_" + (end - start)  + "ms");
	}

	public static void searchSimilarData(Mat mat, SquareOutSideData outside){
		double valEva = Double.MAX_VALUE;

		Point pResult = new Point(0, 0);

		for(int i = 1; i < (mat.height() - outside.height()) - 1; i++){
			for(int j = 1; j < (mat.width() - outside.width()) - 1; j++){
				if(outside.pULx == j && outside.pULy == i){
					System.out.println("skip" + j + ", " + i);

				}else{
					SquarePoints searchSqure = new SquarePoints(new Point(j, i),new Point(j + outside.width(), i + outside.height()));
					SquareOutSideData searchOutSideData = new SquareOutSideData(mat, searchSqure);
					double temp = searchOutSideData.calEvaluation(outside);

					if(temp <= valEva){
						valEva = temp;
						pResult = new Point(j, i);
						System.out.println(valEva+ "_" + j + ", " + i);
					}
				}
			}
		}
		Mat subMat = new Mat();

		mat.submat(new Rect(
				(int)pResult.x,
				(int)pResult.y,
				(int)outside.width(),
				(int)outside.height())).copyTo(subMat);;
		Imgcodecs.imwrite("./result.png", subMat);
	}
}