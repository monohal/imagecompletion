import org.opencv.core.Mat;

public class SquareOutSideData{
	SquarePoints redPoints;
	int pULx;
	int pULy;
	int pDRx;
	int pDRy;

	Mat mat;

	LineData lineDataTop;
	LineData lineDataRight;
	LineData lineDataLeft;
	LineData lineDataBottom;

	/**
	 * (pULx, pULy)-	redWidth	-(pDRx, pULy)
	 * 		|
	 * 	redHeight
	 * 		|
	 * (pULx, pDRy)					  (pDRx, pDRy)
	 *
	 *
	 * @param mat
	 * @param redPoints
	 */
	public SquareOutSideData(Mat mat, SquarePoints redPoints) {
		this.mat = mat;
		this.redPoints = redPoints;

		pULx = (int)redPoints.pUL.x;
		pULy = (int)redPoints.pUL.y;
		pDRx = (int)redPoints.pDR.x;
		pDRy = (int)redPoints.pDR.y;

		lineDataTop = new LineData();
		lineDataBottom = new LineData();
		lineDataRight = new LineData();
		lineDataLeft = new LineData();

		setData();
	}

	private void setData(){
		for(int i = 0; i <= this.redPoints.width(); i++){
			lineDataTop.setData(mat.get(pULy - 1, pULx + i));
			lineDataBottom.setData(mat.get(pDRy + 1, pULx + i));
		}

		for(int i = 0; i <= this.redPoints.height(); i++){
			lineDataRight.setData(mat.get(pULy + i, pDRx + 1));
			lineDataLeft.setData(mat.get(pULy + i, pULx - 1));
		}
	}

	public void printData(){
		System.out.printf("TopLineData\n");
		for(Double[] data : lineDataTop.data){
			System.out.printf("%f,%f,%f\n",(double)data[0], (double)data[1], (double)data[2]);
		}

		System.out.printf("BottomLineData\n");
		for(Double[] data : lineDataBottom.data){
			System.out.printf("%f,%f,%f\n",(double)data[0], (double)data[1], (double)data[2]);
		}

		System.out.printf("RightLineData\n");
		for(Double[] data : lineDataRight.data){
			System.out.printf("%f,%f,%f\n",(double)data[0], (double)data[1], (double)data[2]);
		}

		System.out.printf("LeftLineData\n");
		for(Double[] data : lineDataLeft.data){
			System.out.printf("%f,%f,%f\n",(double)data[0], (double)data[1], (double)data[2]);
		}

		System.out.println();
	}

	public double calEvaluation(SquareOutSideData sosd){
		double valEva = 0;

		for(int i = 0; i < lineDataTop.data.size(); i++){
			for(int j = 0; j < 3; j++){
				valEva += Math.pow((double)lineDataTop.data.get(i)[j] - (double)sosd.lineDataTop.data.get(i)[j], 2);
			}
		}

		for(int i = 0; i < lineDataBottom.data.size(); i++){
			for(int j = 0; j < 3; j++){
				valEva += Math.pow((double)lineDataBottom.data.get(i)[j] - (double)sosd.lineDataBottom.data.get(i)[j], 2);
			}
		}

		for(int i = 0; i < lineDataRight.data.size(); i++){
			for(int j = 0; j < 3; j++){
				valEva += Math.pow((double)lineDataRight.data.get(i)[j] - (double)sosd.lineDataRight.data.get(i)[j], 2);
			}
		}

		for(int i = 0; i < lineDataLeft.data.size(); i++){
			for(int j = 0; j < 3; j++){
				valEva += Math.pow((double)lineDataLeft.data.get(i)[j] - (double)sosd.lineDataLeft.data.get(i)[j], 2);
			}
		}
		return valEva;
	}

	public double width(){
		return redPoints.width();
	}

	public double height(){
		return redPoints.height();
	}
}
